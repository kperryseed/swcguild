function validateForm()
{
	var name = document.forms["contactus"]["name"].value;
	var email = document.forms["contactus"]["email"].value;
	var phone = document.forms["contactus"]["phone"].value;
	var reason = document.getElementById("reasons")[document.getElementById("reasons").selectedIndex].value;
	var addinfo = document.forms["contactus"]["addinfo"].value;
	var contactdays = document.forms["contactus"]["contactmonday"].checked||document.forms["contactus"]["contacttuesday"].checked||
	document.forms["contactus"]["contactwednesday"].checked||document.forms["contactus"]["contactthursday"].checked||
	document.forms["contactus"]["contactfriday"].checked;
	
		if (name ==""){
		alert("Please enter your name.");
		return false;
	}
	if (email =="" && phone =="") {
		alert("We need either an email address or phone number to contact you.");
		return false;
	}
	if (reason == "other" && addinfo ==""){
		alert("Please provide some details in the Additional Information.");
		return false;
	}
	if (contactdays == false){
		alert("Please indicate a day that is best for us to contact you.");
		return false;
	}
	return true;
	};